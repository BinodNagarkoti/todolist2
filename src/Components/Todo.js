import React from "react";
import Button from "@material-ui/core/Button";

function Todos(props) {
  return (
    <div>
      <h3> List of Project </h3>
      {props.project.map((item, index) => (
        <div key={index}>
          <li>
            {item.title} : {item.category}
            <Button
              id="del"
              color="secondary"
              onClick={() => props.onDelete(index)}
            >
              Delete
            </Button>
            <Button
              id="edit"
              color="primary"
              onClick={() => props.onEdit(index)}
            >
              Edit
            </Button>
          </li>
        </div>
      ))}
    </div>
  );
}
export default Todos;
