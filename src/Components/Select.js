import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

export default function SimpleSelect({ id, value, category, onChange }) {
  return (
    <div>
      <InputLabel id="demo-simple-select-label">Category</InputLabel>
      <Select id={id} value={value} onChange={onChange}>
        {category.map((value, index) => (
          <MenuItem key={index} value={value.name}>
            {value.name}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
}
