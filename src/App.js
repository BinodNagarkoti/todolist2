import React, { Component } from "react";
import "./App.css";
// import uuid from "uuid";
import Grid from "@material-ui/core/Grid";
import { Input } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Todos from "./Components/Todo";
import Select from "./Components/Select";
import $ from "jquery";

class App extends Component {
  constructor() {
    super();
    this.state = {
      updateId: "",
      project: [
        {
          title: "Learn ReactJs",
          category: "JavaScript"
        },
        {
          title: "Website  of Business",
          category: "Facebook "
        },
        {
          title: "E-commerce Shopping",
          category: "Web Development"
        }
      ],
      category: [
        {
          id: 1,
          name: "JavaScript"
        },
        {
          id: 2,
          name: "Facebook"
        },
        {
          id: 3,
          name: "Web Development"
        }
      ],
      crudToggle: false,
      selectedCategory: "",
      task: "",
      todos: []
    };
    this.getListItem = this.getListItem.bind(this);
    this.getTodos = this.getTodos.bind(this);
    this.getFromFetchMethod = this.getFromFetchMethod.bind(this);

    this.onSelect = this.onSelect.bind(this);
    // this.onDelete = this.onDelete.bind(this);

    this.onChange = this.onChange.bind(this);
    this.handleAction = this.handleAction.bind(this);
  }

  getTodos() {
    $.ajax({
      url: "https://jsonplaceholder.typicode.com/posts",
      dataType: "json",
      cache: false,
      success: function(data) {
        this.setState({ todos: data });
      }.bind(this),
      error: function(xhr, status, err) {
        console.log(err);
      }
    });
  }
  handleAction(e) {
    // e.preventDefault();
    const { id } = e.target;
    if (!this.state.crudToggle) {
      console.log(id);
      this.setState(prevState => {
        const newProject = {
          title: prevState.task,
          category: prevState.selectedCategory
        };
        return { project: prevState.project.concat(newProject) };
      });
    }
  }

  onChange = ({ target }) => {
    const { value, name } = target;

    this.setState({
      ...this.state,
      [name]: value
    });
  };
  onSelect = e => {
    const { value } = e.target;
    this.setState({ selectedCategory: value });
  };
  onDelete = id => {
    const newList = this.state.project;
    newList.splice(id, 1);
    this.setState({ project: newList });
  };
  onEdit = id => {
    const { title, category } = this.state.project[id];
    this.setState({
      updateId: id,
      task: title,
      selectedCategory: category,
      crudToggle: true
    });
  };
  onUpdate = () => {
    const id = this.state.updateId;
    console.log(id);

    const title = this.state.task;
    const category = this.state.selectedCategory;
    //to maintain an state in react array.map() is greate rescue
    this.setState(prevState => {
      const project = prevState.project.map((item, j) => {
        if (j === id) {
          return { title: title, category: category };
        } else {
          return item;
        }
      });
      return {
        task: "",
        selectedCategory: "",
        crudToggle: false,
        updateId: "",
        project
      };
    });

    // const newList = { ...this.state.project };
    // console.log("oldList:", newList);

    // newList[id] = { title: title, category: category };
    // console.log("newList:", newList);
    // this.setState(
    //   prevState => ({
    //     project: prevState.project[id]({ title: title, category: category }),
    //     crudToggle: false,
    //     updateId: ""
    //   }),
    //   console.log("Success", console.log(this.state.project[id]))
    // );
  };
  getListItem = () => {
    console.log(this.state.project);
  };

  componentDidMount() {
    // this.getTodos();
    this.getFromFetchMethod();
  }
  render() {
    return (
      <div className="App">
        <Grid container spacing={10}>
          <Grid item xs={12}>
            <h1> Todo List Part 2</h1>
          </Grid>
        </Grid>
        <Grid container spacing={4}>
          <Grid item xs={2}>
            <Input
              type="text"
              name="task"
              ref="inputRef"
              id="text"
              placeholder="Enter Task"
              onChange={this.onChange}
              value={this.state.task}
            />
          </Grid>
          <Grid item xs={2}>
            <Select
              id="select"
              name="category"
              onChange={this.onSelect}
              value={this.state.selectedCategory}
              category={this.state.category}
            />
          </Grid>
          <Grid item xs={8}>
            {!this.state.crudToggle ? (
              <Button
                id="btn-add"
                variant="contained"
                disabled={this.state.task === "" ? true : false}
                onClick={this.handleAction}
                color="primary"
              >
                Add New Task
              </Button>
            ) : (
              <Button
                id="btn-edit"
                variant="contained"
                onClick={this.onUpdate}
                color="primary"
              >
                Update Task
              </Button>
            )}
          </Grid>
          <Grid item xs={12}>
            <Todos
              project={this.state.project}
              onDelete={this.onDelete.bind(this)}
              onEdit={this.onEdit.bind(this)}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <h3> API todo list</h3>

          <table>
            <thead>
              <tr>
                <th align="center">ID</th>
                <th align="center">Title</th>
                <th align="center">Completed</th>
              </tr>
            </thead>
            {this.state.todos.map((row, index) => (
              <tbody key={index}>
                <tr>
                  <td> {row.id}</td>
                  <td> {row.title}</td>
                  <td> {row.completed ? "true" : "false"}</td>
                </tr>
              </tbody>
            ))}
          </table>
        </Grid>
      </div>
    );
  }
  getFromFetchMethod() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ todos: data });
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export default App;
